param (
    [Parameter(Mandatory)][string] $srcDir,
    [string] $matchPackages,
    [switch] $preferVersionInTargets,
    [switch] $allowTargetInclude,
    [switch] $includeCache
)

#Requires -Version 5.0

$ErrorActionPreference = "Stop"

. "$PSScriptRoot/PackageReferenceType.ps1"
. "$PSScriptRoot/PackageReference.ps1"
. "$PSScriptRoot/PackageConfig.ps1"
. "$PSScriptRoot/LintLogger.ps1"
. "$PSScriptRoot/Packages.ps1"
. "$PSScriptRoot/PackageCache.ps1"

# reset the warning/error logger and package cache
[LintLogger]::Reset()
[PackageCache]::Reset()

# read package refs from project and directory target files
[PackageReference[]] $refs = [PackageReference]::GetReferences($srcDir, @("*.csproj", "Directory.Build.targets"), $matchPackages, $true)

# read package refs from packages.config files
$refs += [PackageConfig]::GetReferences($srcDir, @("packages.config"), $matchPackages, $true)

# read the dependant packages from the cache
if ($includeCache) {
    $refs += [PackageCache]::GetReferences($refs, $matchPackages)
}

[Packages] $packages = [Packages]::new(($refs | Where-Object { $_ }))

# warn on package version info in the project file
if ($preferVersionInTargets) {
    $packages.
        Where({ ($_.name -NotMatch "^PureGym\.") -and $_.IsProject() -and $_.HasVersion() }).
        Error('"$($_.name)" in project file "$($_.relativeSourceFile)" has version "$($_.version)", consider moving to a targets file') | `
        Write-Lint
}

# error on PureGym package version info in the project or config file
$packages.
    Where({ ($_.name -Match "^PureGym\.") -and $_.HasVersion() }).
    Where({ $_.IsProject() -or $_.IsConfig() }).
    Error('"$($_.name)" in file "$($_.relativeSourceFile)" has version "$($_.version)", move to a targets file') | `
    Write-Lint

# error on missing package version info in the targets file
$packages.
    Where({ $_.IsTarget() -and !$_.HasVersion() }).
    Error('"$($_.name)" in targets file "$($_.relativeSourceFile)" is missing a version attribute') | `
    Write-Lint

# error on missing package version info in the targets and project files
$packages.
    Where({ $_.IsTarget() -or ($_.IsProject -and !$_.HasVersion()) }).
    GroupBy('name') | `
    ForEach-Object {
        if (!($_.value | Where-Object { $_.IsTarget() -and $_.HasVersion() })) {
            [string[]] $files = ($_.Value | ForEach-Object { "'$($_.relativeSourceFile)'" })
            "Error: '$($_.Key)' has no version specified in any of " + ($files -join " and`n  ")
        }
    } | `
    Write-Lint

# error on package include in the targets file
if (!$allowTargetInclude) {
    $packages.
        Where({ $_.IsTarget() -and $_.IsInclude() }).
        Error('found include for package "$($_.name)" in targets file "$($_.relativeSourceFile)", this should be Update') | `
        Write-Lint
}

# warn on legacy package.config files
$packages.
    Where({ $_.IsConfig() }).
    GroupBy('relativeSourceFile') | `
    ForEach-Object { 
        [string] $file = $_.Key
        "Warning: legacy package configuration file '$file' found, consider moving to PackageReference"
    } | `
    Write-Lint

# warn on multiple package version directives
$packages.
    Where({ $_.HasVersion() }).
    GroupBy('name', 'version') | `
    Where-Object { ($_.value | ForEach-Object { $_.version } | Get-Unique).Count -gt 1 } | `
    ForEach-Object { 
        [string] $name = $_.Key
        [string[]] $files = ($_.Value | ForEach-Object { "'$($_.relativeSourceFile)' as '$($_.version)'" })
        "Warning: '$name' appears in files " + ($files -join " and`n  ") 
    } | `
    Write-Lint

# warn on package cache issues
[PackageCache]::GetErrors() | `
    ForEach-Object { "Warning: $($_)" } | `
    Write-Lint

Write-Host "`nWarnings: $([LintLogger]::warnings), Errors: $([LintLogger]::errors)`n"
