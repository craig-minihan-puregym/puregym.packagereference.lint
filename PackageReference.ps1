$ErrorActionPreference = "Stop"

. "$PSScriptRoot/PackageReferenceType.ps1"

class PackageReference {
    [string] $name
    [string] $version
    hidden [PackageReferenceType] $refType
    [System.IO.FileInfo] $sourceFile
    [string] $relativeSourceFile

    PackageReference([string] $name, [string] $version, [PackageReferenceType] $refType, [System.IO.FileInfo] $sourceFile, [string] $relativeSourceFile) {
        $this.name = $name
        $this.version = [PackageReference]::GetPackageVersion($version)
        $this.refType = $refType
        $this.sourceFile = $sourceFile
        $this.relativeSourceFile = $relativeSourceFile
    }

    static [PackageReference[]] GetReferences([string] $srcDir, [string[]] $includeFiles, [string] $matchPackages = $null, [bool] $recurse = $true) {
        [string] $absDir = Resolve-Path $srcDir

        return Get-ChildItem -Path $srcDir -Recurse:$recurse -File -Include $includeFiles | ForEach-Object {
            [System.IO.FileInfo] $sourceFile = $_
            [xml] $xml = Get-Content $sourceFile | ForEach-Object { $_.Replace('xmlns=', '_xmlns=') }
            $xml.SelectNodes('//ItemGroup/PackageReference') | ForEach-Object {
                [string] $name = if ($_.Include) { $_.Include } else { $_.Update }
                if (!$matchPackages -or ($name -match $matchPackages)) {
                    [PackageReferenceType] $refType = if ($_.Update) { [PackageReferenceType]::Update } else { [PackageReferenceType]::Include }
                    [string] $version = $_.Version
                    [string] $relativeSourceFile = $sourceFile.FullName.Replace($absDir, ".")
                    [PackageReference]::new($name, $version, $refType,  $sourceFile, $relativeSourceFile)
                }
            }
        }
    }

    [bool] IsInclude() {
        return $this.refType -eq [PackageReferenceType]::Include
    }

    [bool] IsUpdate() {
        return $this.refType -eq [PackageReferenceType]::Update
    }

    [bool] IsDependency() {
        return $this.refType -eq [PackageReferenceType]::Dependency
    }

    [bool] IsProject() {
        return $this.sourceFile.Extension -eq ".csproj"
    }

    [bool] IsTarget() {
        return $this.sourceFile.Extension -eq ".targets"
    }

    [bool] IsConfig() {
        return $this.sourceFile.Extension -eq ".config"
    }

    [bool] IsNuSpec() {
        return $this.sourceFile.Extension -eq ".nuspec"
    }

    [bool] HasVersion() {
        return !!$this.version
    }

    static hidden [string] GetPackageVersion([string] $version) {
        if ($version) {
            [string[]] $parts = $version -split '\-'

            [int] $maxMajMinPatchParts = 3
            [string[]] $majMinPatchParts = $parts[0] -split '\.'

            while ($majMinPatchParts.Count -lt $maxMajMinPatchParts) {
                $majMinPatchParts += '0'
            }

            if (($majMinPatchParts.Count -gt $maxMajMinPatchParts) -and ($majMinPatchParts[3] -ne '0')) {
                $maxMajMinPatchParts = 4;
            }

            $parts[0] = ($majMinPatchParts | Select-Object -first $maxMajMinPatchParts) -join '.'
            $local:version = $parts -join '-'
        }

        return $version
    }
}
