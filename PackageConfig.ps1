$ErrorActionPreference = "Stop"

. "$PSScriptRoot/PackageReference.ps1"

class PackageConfig : PackageReference {
    PackageConfig([string] $name, [string] $version, [System.IO.FileInfo] $sourceFile, [string] $relativeSourceFile) : 
        base($name, $version, [PackageReferenceType]::Include, $sourceFile, $relativeSourceFile) {
    }

    static [PackageConfig[]] GetReferences([string] $srcDir, [string[]] $includeFiles, [string] $matchPackages = $null, [bool] $recurse = $true) {
        [string] $absDir = Resolve-Path $srcDir

        return Get-ChildItem -Path $srcDir -Recurse:$recurse -File -Include $includeFiles | ForEach-Object {
            [System.IO.FileInfo] $sourceFile = $_
            [xml] $xml = Get-Content $sourceFile | ForEach-Object { $_.Replace('xmlns=', '_xmlns=') }
            $xml.SelectNodes('//packages/package') | ForEach-Object {
                [string] $name = $_.id
                if (!$matchPackages -or ($name -match $matchPackages)) {
                    [string] $version = $_.version
                    [string] $relativeSourceFile = $sourceFile.FullName.Replace($absDir, ".")
                    [PackageConfig]::new($name, $version, $sourceFile, $relativeSourceFile)
                }
            }
        }
    }
}