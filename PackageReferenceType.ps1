$ErrorActionPreference = "Stop"

enum PackageReferenceType {
    Include = 1
    Update = 2
    Dependency = 3
}