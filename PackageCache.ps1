$ErrorActionPreference = "Stop"

. "$PSScriptRoot/PackageReferenceType.ps1"
. "$PSScriptRoot/PackageReference.ps1"

class PackageCacheException : Exception {
    [string] $name
    [string] $version
    [string] $specPath

    PackageCacheException([string] $name, [string] $version, [string] $specPath, [Exception] $innerException) : 
        base("can't read package '${name}', version '${version}' in the cache, expected a valid spec file at '${specPath}'", $innerException) 
    {
        $this.name = $name
        $this.version = $version
        $this.specPath = $specPath
    }
}

class PackageCache {
    static hidden [System.Collections.Generic.Dictionary[string, xml]] $cache = @{}
    static hidden [string] $cachePath = "$($env:USERPROFILE)/.nuget/packages/"
    static hidden [string[]] $errors = @()

    static [void] Reset() {
        [PackageCache]::cache = @{}
        [PackageCache]::errors = @()
    }

    static hidden [bool] IsCached([string] $packagePath) {
        return [PackageCache]::cache.ContainsKey($packagePath)
    }

    static hidden [xml] GetDefinition([string] $packagePath) {
        [xml] $spec = [PackageCache]::cache[$packagePath]
        if (!$spec) {            
            $spec = Get-Content $packagePath | ForEach-Object { $_.Replace('xmlns=', '_xmlns=') }
            [PackageCache]::cache[$packagePath] = $spec
        }
        return $spec
    }

    static [PackageReference[]] GetReferences([string] $name, [string] $version, [string] $matchPackages = $null) {
        [string] $specPath = [PackageCache]::GetNuSpecPath($name, $version)

        if (![PackageCache]::IsCached($specPath)) {
            try {
                [xml] $spec = [PackageCache]::GetDefinition($specPath)

                return $spec.SelectNodes('/package/metadata/dependencies/dependency') | ForEach-Object {
                    [string] $local:name = $_.id
                    if (!$matchPackages -or ($local:name -match $matchPackages)) {
                        [string] $local:version = $_.version
                        if ($local:version -and ($local:version[0] -ne '[')) {
                            return [PackageReference]::new($local:name, $local:version, [PackageReferenceType]::Dependency, $specPath, $specPath)
                        }
                    }
                }
            } catch {
                throw [PackageCacheException]::new($name, $version, $specPath, $_.Exception)
            }
        } else {
            return @()
        }        
    }

    static [PackageReference[]] GetReferences([PackageReference[]] $refs, [string] $matchPackages = $null) {
        return $refs | ForEach-Object {
            if ($_ -and $_.HasVersion()) {
                try {
                    return [PackageCache]::GetReferences($_.name, $_.version, $matchPackages)
                } catch {
                    [PackageCache]::errors += $_.Exception.Message
                    return @()
                }
            }
        }
    }

    static [string[]] GetErrors() {
        return [PackageCache]::errors
    }

    static hidden [string] GetNuSpecPath([string] $name, [string] $version) {
        return "$([PackageCache]::cachePath)/${name}/${version}/${name}.nuspec"
    }
}