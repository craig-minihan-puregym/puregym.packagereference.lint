# PackageReference Linter
A simple script to look for problems with package referenences in a repository tree.

### Warns on:
* package versions specified in `csproj` or `packages.config` files (optional)
* multiple versions of the same package
* legacy `packages.config` file found
* missing packages in the cache (if enabled)

### Errors on:
* PureGym packages versioned in `csproj` or `packages.config` files
* Missing package version across `csproj` and `packages.config` files (i.e. package referenced but no version supplied)
* `Include` directive in `target` files
* missing version numbers in `target` files

## Command Line
| Parameter | Description |
|-----------|-------------|
| -srcDir   | An absolute or relative path to a source directory or repository root |
| -matchPackages | A regular expression to filter packages by name |
| -preferVersionInTargets | Generate warnings when *any* version information is specificed in a `csproj` or `package.config` file |
| -allowTargetInclude | Don't generate an error when an `Include` directive is found in a targets file |
| -includeCache | Scan package dependencies in the cache |

## Example
```ps1
.\pr-lint.ps ..\puregym.site\src -matchPackages "^PureGym\."
```

### Missing Features
* Checking assembly bindings and redirects
* version ranges handling anywhere
* fixing problems
* integrating with CI platforms
* unit tests
* break out scripts into modules