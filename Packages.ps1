$ErrorActionPreference = "Stop"

. "$PSScriptRoot/PackageReference.ps1"

class Packages {
    hidden [PackageReference[]] $packages

    Packages([PackageReference[]] $packages) {
        $this.packages = $packages
        if ($this.packages -eq $null) { 
            $this.packages = @()
        }
    }

    [System.Collections.IEnumerator] GroupBy([string[]] $fields) {
        return $this.GroupBy($fields, $null, $false)
    }

    [System.Collections.IEnumerator] GroupBy([string[]] $fields, [string] $thenBy) {
        return $this.GroupBy($fields, $thenBy, $false)
    }

    [System.Collections.IEnumerator] GroupBy([string[]] $fields, [string] $thenBy, [bool] $thenByUnique) {
        [bool] $keyAsString = $fields.Count -gt 1
        [System.Collections.Hashtable] $hash = ($this.packages | Group-Object -AsHashTable -Property $fields -AsString:$keyAsString)
        if (($hash -ne $null) -and $thenBy) {
            [System.Collections.Hashtable] $thenByHash = @{}
            $hash.Keys | ForEach-Object { $thenByHash[$_] = ($hash[$_] | Sort-Object -Property $thenBy -Unique:$thenByUnique) } | Out-Null
            $hash = $thenByHash
        }

        $hash = if ($hash -ne $null) { $hash } else { @{} }

        return $hash.GetEnumerator()
    }

    [Packages] Where([ScriptBlock] $where) {
        return [Packages]::new(($this.packages | Where-Object -FilterScript $where))
    }

    [string[]] Error([string] $msg) {
        return ($this.packages | ForEach-Object { [string] $text = $ExecutionContext.InvokeCommand.ExpandString($msg); "Error: ${text}" })
    }

    [string[]] Warning([string] $msg) {
        return ($this.packages | ForEach-Object { [string] $text = $ExecutionContext.InvokeCommand.ExpandString($msg); "Warning: ${text}" })
    }

    [System.Collections.IEnumerator] GetEnumerator() {
        return $this.packages.GetEnumerator()
    }
}
