$ErrorActionPreference = "Stop"

class LintLogger {
    static [int] $errors
    static [int] $warnings

    static [void] Write([string] $msg) {
        [ConsoleColor] $colour = [System.ConsoleColor]::Gray

        if ($msg.StartsWith("Warning:")) { 
            $colour = [System.ConsoleColor]::Yellow 
            ++[LintLogger]::warnings
        }
        elseif ($msg.StartsWith("Error:")) {
            $colour = [System.ConsoleColor]::Red
            ++[LintLogger]::errors
        }
        Write-Host -ForegroundColor $colour $msg
    }

    static [void] Reset() {
        [LintLogger]::errors = 0
        [LintLogger]::warnings = 0
    }
}

function Write-Lint {
    [cmdletbinding()] param ([Parameter(ValueFromPipeline)][string] $item)
    process {
        if ($_) {
            [LintLogger]::Write($_)
        }
    }
}